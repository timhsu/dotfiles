# Dotfiles

My personal dotfiles.


## Usage

I'm using the "bare git repo" method to manage the dotfiles. See tutorials at https://www.atlassian.com/git/tutorials/dotfiles.


### Install the dotfiles onto a new system

Suppose `$HOME/dotfiles` would be used to track the dotfiles, then first make the following alias:
```bash
alias dotgit="/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME"
```

Next, clone the git repo into the folder:
```bash
git clone --bare <git-repo-url> $HOME/dotfiles
```

Run `dotgit checkout`. You might see conflicts between the git-tracked files and the files already in the system. For example:
```bash
error: The following untracked working tree files would be overwritten by checkout:
    .bashrc
    .profile
Please move or remove them before you can switch branches.
Aborting
```
If so, move or remove them, and rerun `dotgit checkout`.

Lastly, set the flag `showUntrackedFiles` to `no`:
```bash
dotgit config --local status.showUntrackedFiles no
```


## Packages to install

The following packages are required for the dotfiles or are very often used. Their names are based on the Arch distribution.

Tip: copy the following package names into a file (say, `pkglist.txt`), and install them with `pacman -S --needed - < pkglist.txt`


### Main packages

```
alacritty
bat
fd
fish
flameshot
fzf
keychain
lazygit
mpv
neovim
npm
ranger
redshift
ripgrep
rofi
starship
sxhkd
sxiv
w3m
xclip
xcolor
xdg-user-dirs
zathura
zsh
zsh-autosuggestions
zsh-history-substring-search
zsh-syntax-highlighting
noto-fonts-emoji
ttf-iosevka-nerd
ttf-jetbrains-mono-nerd
```


### AUR packages

```
yay
dragon-drop
```


## External resources

```
nvchad.com
```


## Notes

- `zathura` might also need `zathura-pdf-poppler` or `zathura-pdf-mupdf` for opening PDF.
- `npm` is used for installing language servers to support `neovim`'s LSP.
- `fish` shell extensions may not be enabled by default depending on the package manager.
- Check out `~/.config/ranger/scope.sh` or `pacman -Si ranger` for `ranger`'s optional dependencies.
- Manually running `xdg-users-dirs-update` may be neccessary when migrating to a new system.
- Look up `fcitx5` for additional language input.
- Look up `libinput-gestures` for additional touchpad support.
- __Never track the ssh files, but always remember to port them when migrating to a new system.__
