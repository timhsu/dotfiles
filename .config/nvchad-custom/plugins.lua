local plugins = {
  -- Use Mason for managing LSP serves, formatters, and other tools
 {
   "williamboman/mason.nvim",
   opts = {
      ensure_installed = {
        "bash-language-server",
        "pyright",
      },
    },
  },
  -- In order to modify the `lspconfig` configuration:
  {
    "neovim/nvim-lspconfig",
     config = function()
        require "plugins.configs.lspconfig"
        require "custom.configs.lspconfig"
     end,
  },
}
return plugins
