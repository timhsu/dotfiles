---@type ChadrcConfig
local M = {}

M.ui = { theme = 'onedark' }

-- Tell NvChad where the custom plugins live
M.plugins = "custom.plugins"

return M
