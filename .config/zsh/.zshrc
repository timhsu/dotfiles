# Get compinit and colors
autoload -Uz compinit colors
compinit
colors

# History settings
HISTFILE="$HOME"/.local/share/zsh_history
HISTSIZE=2000
SAVEHIST=2000

# Shell options
setopt autocd                                                   # If only directory path is entered, cd there
setopt appendhistory                                            # Immediately append history instead of overwriting
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt rcexpandparam                                            # Array expansion with parameters
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true                              # Automatically find new executables in path
WORDCHARS=${WORDCHARS//\/[&.;]}                                 # Don't consider certain characters part of the word

# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/zsh_cache

# Aliases
alias condago='eval "$($CONDA_PATH/bin/conda shell.zsh hook)"'
alias dotgit="/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME"
alias fgitlog="git log --oneline | fzf -m --preview 'git show --color {+1}'"
alias ls="ls --color=auto"
alias grep="grep --color=auto"
alias ll="ls -lh"
alias la="ls -la"

# zsh plugins (need to install them separately)
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Keybindings
bindkey -e                                      # Use emacs keybindings
bindkey '^[[A'    history-substring-search-up   # up
bindkey '^[[B'    history-substring-search-down # down
bindkey '^[[1;5D' backward-word                 # ctrl-left
bindkey '^[[1;5C' forward-word                  # ctrl-right
bindkey '^[[1;3D' backward-word                 # alt-left
bindkey '^[[1;3C' forward-word                  # alt-right
bindkey '^H'      backward-kill-word            # ctrl-backspace

# Key autorepeat speed
[[ $(command -v xset) ]] && xset r rate 200 50

# fzf keybindings
[[ -f /usr/share/fzf/key-bindings.zsh ]] && source /usr/share/fzf/key-bindings.zsh

# Starship prompt
[ $(command -v starship) ] && eval "$(starship init zsh)"
