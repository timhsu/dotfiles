# Secondary prefix
set-option -g prefix C-Space
bind-key C-Space send-prefix

# Split panes using \ and -, at the same working directory
bind "\\" split-window -h -c "#{pane_current_path}"
bind "-"  split-window -v -c "#{pane_current_path}"

# Enable mouse mode (tmux 2.1 and above)
set -g mouse on

# 256 colours
set -g default-terminal "screen-256color"

# Switch panes using Alt-arrow without prefix
bind -n M-h select-pane -L
bind -n M-l select-pane -R
bind -n M-k select-pane -U
bind -n M-j select-pane -D

# Resize panes
bind -r C-h resize-pane -L 5
bind -r C-l resize-pane -R 5
bind -r C-k resize-pane -U 5
bind -r C-j resize-pane -D 5

# Shift arrow to switch windows
bind -n S-Left  previous-window
bind -n S-Right next-window

# Reload config file
bind r source-file ~/.tmux.conf \; display-message "Reloaded ~/.tmux.conf"

# Status line appearance
active_window_bg=colour32
inactive_window_bg=colour102
bar_bg=colour237
bar_fg=colour255

set  -g status-style bg=$bar_bg
setw -g window-status-style fg=$bar_fg
setw -g window-status-current-format ' #I #W '
setw -g window-status-format ' #I #W '
setw -g window-status-current-style bg=$active_window_bg
