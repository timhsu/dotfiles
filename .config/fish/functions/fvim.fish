function fvim --description "Fuzzy search a file and then open with vim/nvim"
    switch $argv
        case "*/"
            set files (fd . $argv --type f --hidden --follow --exclude .git)
        case "*"
            set files (fd   $argv --type f --hidden --follow --exclude .git --full-path)
    end

    set fzfcmd fzf -0 -1 -m \
                   --preview 'bat --plain --color=always {}' \
                   --preview-window=60%

    set choice (string split ' ' $files | $fzfcmd)
    if test -z "$choice"
        echo "Nothing selected"
    else
        nvim $choice
    end
end
