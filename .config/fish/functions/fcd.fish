function fcd -d "cd to recently visited directories using fzf"
    # Inspired from https://github.com/fish-shell/fish-shell/issues/2847

    set -l all_dirs $dirprev $dirnext
    if not set -q all_dirs[1]
        echo 'No previous directories to select. You have to cd at least once.'
        return 0
    end

    # Reverse the directories so the most recently visited is first in the list.
    # Also, eliminate duplicates; i.e., we only want the most recent visit to a
    # given directory in the selection list.
    set -l uniq_dirs
    for dir in $all_dirs[-1..1]
        if not contains $dir $uniq_dirs
            set uniq_dirs $uniq_dirs $dir
        end
    end

    set -l choice (string split ' ' $uniq_dirs | fzf --prompt='cd to: ')
    if test -z "$choice"
        echo "Nothing selected"
    else
        cd $choice
    end
end
