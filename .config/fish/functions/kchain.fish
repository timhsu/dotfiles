function kchain --description "Wrapper function for 'keychain --eval' to work in fish shell"
    set -l CURRENT_SHELL "$SHELL"
    set -x SHELL fish
    eval (keychain --eval $argv)
    set -x SHELL "$CURRENT_SHELL"
end
