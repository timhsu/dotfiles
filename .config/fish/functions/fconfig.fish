function fconfig --description "Select git-tracked dotfiles via fzf and open with vim/nvim"
    pushd $HOME

    set fzfcmd fzf -m \
                   --bind ctrl-a:select-all,ctrl-d:deselect-all,ctrl-t:toggle-all \
                   --preview 'bat --plain --color=always {}' \
                   --preview-window=60%

    set choice (dotgit ls-files | $fzfcmd)
    if test -z "$choice"
        echo "Nothing selected"
    else
        nvim $choice
    end

    popd
end
