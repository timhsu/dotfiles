# Aliases
alias condago="eval $CONDA_PATH/bin/conda "shell.fish" "hook" $argv | source"
alias dotgit="/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME"
alias fgitlog="git log --oneline | fzf -m --preview 'git show --color {+1}'"
alias repo-rsync="rsync -avhP --delete-after --exclude='/.git' --filter='dir-merge,- .gitignore'"

# Enable fzf optional keybindings
[ -f /usr/share/fish/vendor_functions.d/fzf_key_bindings.fish ] && fzf_key_bindings

# Key autorepeat speed
[ (command -v xset) ] && xset r rate 200 50

# Starship prompt
[ (command -v starship) ] && starship init fish | source
