if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
    echo "Downloading junegunn/vim-plug to manage plugins..."
    silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
    silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
                \ > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
    autocmd VimEnter * PlugInstall
endif

" Plugins (vim-plug)
call plug#begin('~/.config/nvim/autoload/plugged')
Plug 'tpope/vim-commentary'         " Comment stuff out
Plug 'tpope/vim-fugitive'           " Git integration
Plug 'tpope/vim-surround'           " Change surrounds
Plug 'itchyny/lightline.vim'        " Lightline status line
Plug 'preservim/nerdtree'           " Tree explore plugin
Plug 'junegunn/fzf'                 " Find fzf executable for vim
Plug 'junegunn/fzf.vim'             " Additional fzf functionality
Plug 'junegunn/goyo.vim'            " Distraction-free viewing
Plug 'arcticicestudio/nord-vim'     " Nord colorscheme
Plug 'navarasu/onedark.nvim'        " One Dark colorscheme

" COC
" Plug 'neoclide/coc.nvim', {'branch':'release'}

" LSP and autocompletion
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'
Plug 'ray-x/lsp_signature.nvim'
call plug#end()

" COC
" source ~/.config/nvim/coc.vim

" LSP
luafile ~/.config/nvim/lua/lsp-config.lua
luafile ~/.config/nvim/lua/nvim-cmp.lua
luafile ~/.config/nvim/lua/lsp-signature.lua

" Colorscheme
colorscheme onedark

" Lightline
let g:lightline = {
    \ 'colorscheme': 'one',
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
    \ },
    \ 'component_function': {
    \   'gitbranch': 'FugitiveHead'
    \ },
    \ }


" NERD tree
map <leader>n :NERDTreeToggle<CR>
let g:NERDTreeShowHidden = 1

" Toggle goyo mode
map <leader>z :Goyo<CR>

" FZF
nnoremap <A-f> :Files<CR>
nnoremap <A-g> :GFiles<CR>
nnoremap <A-b> :Buffers<CR>
let g:fzf_preview_window = 'right,50%'
let g:fzf_layout = { 'window': { 'width': 1.0, 'height': 1.0 } }

" Vim-fugitive
nmap <leader>gs :Git<CR>
