let g:mapleader = "\<Space>"        " Define leader key

filetype plugin on                  " Load plugins based on file type
filetype indent on                  " Load indent settings based on file type
syntax enable                       " Enable syntax highlighting

set nowrap                          " No line wrapping
set hidden                          " Required to keep multiple buffers open
set noswapfile                      " No need for swapfiles
set mouse=a                         " Enable mouse support
set showmatch                       " Highlight matching brace
set hlsearch                        " Highlight all search results
set ignorecase                      " Search case insensitively
set incsearch                       " Search strings incrementally
set number                          " Set line number
set backspace=indent,eol,start      " Proper backspace behavior
set autoindent                      " Copy indent from previous line
set showcmd                         " Show current command in status line
set splitbelow splitright           " Split to below and right
set wildmode=longest,list,full      " Configure autocompletion
set tabstop=4                       " Show tabs as 4 spaces
set softtabstop=4                   " Number of spaces for inserting a tab
set shiftwidth=4                    " Number of spaces for indenting
set expandtab                       " Convert tabs into spaces
set clipboard+=unnamedplus          " Use system clipboard
set signcolumn=number               " Display signs in the number column

" Source the configs related to plugins
source $HOME/.config/nvim/plugins.vim

" Toggle hlsearch
noremap <leader>h :set hlsearch!<CR>

" Toggle spell check
noremap <leader>s :setlocal spell!<CR>

" Replace all
nnoremap S :%s//g<Left><Left>

" Remap splits navigation to just ALT + hjkl
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" Make adjusting split sizes a bit more friendly
noremap <silent> <C-Left>  :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up>    :resize +3<CR>
noremap <silent> <C-Down>  :resize -3<CR>

" Change 2 split windows from vert to horiz or horiz to vert
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

" Switch to buffers and tabs
nnoremap <C-n> :bnext<CR>
nnoremap <C-p> :bprev<CR>
nnoremap <A-n> :tabn<CR>
nnoremap <A-p> :tabp<CR>
nnoremap <A-1> :tabnext 1<CR>
nnoremap <A-2> :tabnext 2<CR>
nnoremap <A-3> :tabnext 3<CR>
nnoremap <A-4> :tabnext 4<CR>
nnoremap <A-5> :tabnext 5<CR>
nnoremap <A-6> :tabnext 6<CR>
nnoremap <A-7> :tabnext 7<CR>
nnoremap <A-8> :tabnext 8<CR>
nnoremap <A-9> :tabnext 9<CR>

" Show all buffers in tabs, or close all tabs
let notabs = 1
nnoremap <F8> :let notabs=!notabs<Bar>
                \ :if notabs<Bar>
                    \ :tabo<Bar>
                \ :else<Bar>
                    \ :tab ball<Bar>
                    \ :tabn<Bar>
                \ :endif<CR>

" Better tabbing (indenting)
vnoremap < <gv
vnoremap > >gv

" Ctrl- and Alt- Backspace to delete the previous word in insert mode
imap <C-BS> <C-w>
imap <A-BS> <C-w>

" Paste from the "0 register
nnoremap <leader>p "0p
nnoremap <leader>P "0P
vnoremap <leader>p "0p
vnoremap <leader>P "0P
