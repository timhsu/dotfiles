#!/bin/bash

killall dunst
dunst &

notify-send -u critical "CRITICAL ALERT!"
notify-send -u normal "Normal notification"
notify-send -u low "dunst reloaded"
