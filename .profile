#
# ~/.profile
#

export EDITOR=nvim
export VISUAL=nvim
export PAGER=less
export MANPAGER="less -R --use-color -Dd+g -Du+b"
export LESSHISTFILE=-

# Add paths to external programs
export CONDA_PATH="$HOME/ext/miniconda3"
export PATH="$PATH:$HOME/.local/bin"

# XDG specs
export XDG_CONFIG_HOME="$HOME"/.config
export XDG_CACHE_HOME="$HOME"/.cache
export XDG_DATA_HOME="$HOME"/.local/share

export CARGO_HOME="$XDG_DATA_HOME"/cargo
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export GTK_RC_FILES="$XDG_CONFIG_HOME"/gtk-1.0/gtkrc
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter
export KDEHOME="$XDG_CONFIG_HOME"/kde
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel

# fzf customization
export FZF_DEFAULT_COMMAND="fd --type f --hidden --follow --exclude .git"
export FZF_DEFAULT_OPTS="--layout=reverse --border"
